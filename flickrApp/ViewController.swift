//
//  ViewController.swift
//  flickrApp
//
//  Created by Victoria Alvizurez on 10/18/17.
//  Copyright © 2017 Victoria Alvizurez. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    
    
    @IBOutlet var picSearch: UITextField!
   
   
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "search") {
            // initialize new view controller and cast it as your view controller
            let viewController = segue.destination as! nextViewController
            // your new view controller should have property that will store passed value
            viewController.passedValue = picSearch.text!
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
}

