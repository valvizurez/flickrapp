//
//  nextViewController.swift
//  flickrApp
//
//  Created by Victoria Alvizurez on 10/18/17.
//  Copyright © 2017 Victoria Alvizurez. All rights reserved.
//

import UIKit

class nextViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
     var flickrResults = [AnyObject]()
    struct FlickrPhoto {
        
        let photoId: String
        let farm: Int
        let secret: String
        let server: String
        let title: String
        
        var photoUrl: NSURL {
            return NSURL(string: "https://farm\(farm).staticflickr.com/\(server)/\(photoId)_\(secret)_m.jpg")!
        }
        
    }
    var img = UIImage()
 
    let apiKey = "1dd17dde0fed7286935d83875fcc17dd"
    @IBOutlet var collectionView: UICollectionView!
    
    // self.collectionView.register(UINib(nibName: "photoCell", bundle: nil), forCellWithReuseIdentifier: "cell")
    
      var passedValue = ""
      var search = ""
 //   var pics: [FlickrPhoto] = []
    

    override func viewDidLoad() {
        super.viewDidLoad()
         search = passedValue
        // Do any additional setup after loading the view.
        self.collectionView.register(UINib(nibName: "photoCell", bundle: nil), forCellWithReuseIdentifier: "cell")
        
        let url = flickrSearchURLForSearchTerm(search)
        
        
        let task = URLSession.shared.dataTask(with: url!) { data, response, error in
            guard error == nil else {
                print(error!)
                return
            }
            guard let data = data else {
                print("Data is empty")
                return
            }
            
            let resultsDictionary = try! JSONSerialization.jsonObject(with: data, options: []) as? [String: AnyObject]
            //print(json)
            guard let results = resultsDictionary else{return}
            
            guard let photosContainer = resultsDictionary!["photos"] as? NSDictionary else { return }
           // guard let photosArray = photosContainer["photo"] as? [NSDictionary] else { return }
            self.flickrResults = photosContainer["photo"] as! [AnyObject]
            
            for photoObject in self.flickrResults {
                
                let photoId = photoObject["id"] as? String ?? ""
                let farm = photoObject["farm"] as? Int ?? 0
                let secret = photoObject["secret"] as? String ?? ""
                let server = photoObject["server"] as? String ?? ""
                let title = photoObject["title"] as? String ?? ""
                
                var photoUrl: NSURL {
                    return NSURL(string: "https://farm\(farm).staticflickr.com/\(server)/\(photoId)_\(secret)_m.jpg")!
                }
                
                let url = photoUrl
               
                DispatchQueue.global().async {
                
          
                 let imageData = try? Data(contentsOf: url as URL);
                self.img = UIImage(data: imageData!)!
                   
                  DispatchQueue.main.async {
          
                 //   let  indexPath = IndexPath()
                   // let cell = self.collectionView.dequeueReusableCell(withReuseIdentifier: "cell",
                                                     //             for: indexPath) as! photoCell
                    // cell.myImageView.image = img
                   // cell.mylabel.text = title
                   self.collectionView.reloadData()
               }
               }
              //  }
                
            }
        }
        
        task.resume()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    fileprivate func flickrSearchURLForSearchTerm(_ searchTerm:String) -> URL? {
        
        guard let escapedTerm = searchTerm.addingPercentEncoding(withAllowedCharacters: CharacterSet.alphanumerics) else {
            return nil
        }
        
        let URLString = "https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=\(apiKey)&text=\(escapedTerm)&per_page=20&format=json&nojsoncallback=1"
        
        guard let url = URL(string:URLString) else {
            return nil
        }
        
        return url
    }
    
    
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        return (flickrResults as AnyObject).count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell",
                                                      for: indexPath) as! photoCell
        
        //How to get the image = http://www.flickr.com/services/api/misc.urls.html
        
        cell.myImageView.backgroundColor = UIColor.black
        cell.mylabel.backgroundColor = UIColor.green
        cell.mylabel.text = title
        cell.myImageView.image = img
      //  cell.myImageView.image = UIImage(data: imageData)
        return cell
    }
    
  
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.size.width/2 - 40, height: self.view.frame.size.width/2 - 40)
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
}
